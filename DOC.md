## Documentation avance projet 1

Projet monté en React sur une base Typescript avec bootstrap pour grid CSS

Pour le choix de style j'ai opté pour bootstrap

```
npm install --save bootstrap
```

Nous avons la possibilité de travailler en scss, pour cela nous devrons installer node-sass
```
npm install --save node-sass
```
Nous allons maintenant configurer un `custom.scss` pour y importer nos styles Bootstraps